﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace AATKit
{
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATKitDelegate
    {
        [Export("AATKitHaveAd:")]
        void AATKitHaveAd(NSObject placement);

        [Export("AATKitHaveAdOnMultiSizeBanner:withView:")]
        void AATKitHaveAdOnMultiSizeBanner(NSObject placement, UIView placementView);

        [Export("AATKitNoAds:")]
        void AATKitNoAds(NSObject placement);

        [Export("AATKitShowingEmpty:")]
        void AATKitShowingEmpty(NSObject placement);

        [Export("AATKitPauseForAd")]
        void AATKitPauseForAd();

        [Export("AATKitResumeAfterAd")]
        void AATKitResumeAfterAd();

        [Export("AATKitUserEarnedIncentive")]
        void AATKitUserEarnedIncentive();

        [Export("AATKitUserEarnedIncentiveOnPlacement:")]
        void AATKitUserEarnedIncentiveOnPlacement(NSObject placement);
    }

    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATManagedConsentDelegate
    {
        [Export("CMPNeedsUI:")]
        void managedConsentNeedsUserInterface(NSObject managedConsent);

        [Export("managedConsentCMPFinished")]
        void managedConsentCMPFinished();

        [Export("managedConsentCMPFailedToLoad:error:")]
        void managedConsentCMPFailedToLoad(NSObject managedConsent, String error);

        [Export("managedConsentCMPFailedToShow:error:")]
        void managedConsentCMPFailedToShow(NSObject managedConsent, String error);
    }

    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATVendorConsentDelegate
    {
        [Export("getConsentForNetwork:")]
        NSObject getConsentForNetwork(int network);

        [Export("getConsentForAddApptr")]
        NSObject getConsentForAddApptr();
    }

    //[Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATSimpleConsent
    {
        [Export("initWithNonIABConsent:")]
        void initWithNonIABConsent(int nonIABConsent);
    }

    //[Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATVendorConsent
    {
        [Export("initWithVendorConsentDelegate:")]
        void initWithVendorConsentDelegate(NSObject del);
    }

    //[Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATManagedConsent
    {
        [Export("initWithDelegate:cmp:")]
        void initWithDelegate(NSObject del, NSObject cmp);


    }

    //[Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATCMPGoogle
    {
    }

    //[Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATCMPOgury
    {
        [Export("initWithAssetKey:")]
        void initWithAssetKey(String assetKey);
    }

    //[Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface AATConfiguration
    {
        [Export("alternativeBundleID")]
        string alternativeBundleID { get; set; }

        [Export("initialRules")]
        NSArray initialRules { get; set; }

        [Export("shouldCacheRules")]
        bool shouldCacheRules { get; set; }

        [Export("shouldReportMetricsUsingAlternativeBundleID")]
        bool shouldReportMetricsUsingAlternativeBundleID { get; set; }

        [Export("testModeAccountID")]
        NSNumber testModeAccountID { get; set; }

        [Export("consentRequired")]
        bool consentRequired { get; set; }

        [Export("consent")]
        NSObject consent { get; set; } 

        [Export("shouldUseGeoLocation")]
        bool shouldUseGeoLocation { get; set; }
    }

	[BaseType(typeof(NSObject))]
	interface AATKit
	{}

    delegate void ConsentCompletion();

    [BaseType(typeof(NSObject))]
	interface AATKitXamarin
	{
		[Static]
        [Obsolete("Deprecated, use initWithConfiguration instead.")]
		[Export("init:delegate:")]
		void Init(UIViewController viewController, AATKitDelegate @delegate);

		[Static]
        [Obsolete("Deprecated, use initWithConfiguration instead.")]
		[Export("init:delegate:rulesCaching:initialRules:")]
		void Init(UIViewController viewController, AATKitDelegate @delegate, bool rulesCaching, string initialRules);

        [Static]
        [Export("init:delegate:configuration:")]
        void Init(UIViewController viewController, AATKitDelegate @delegate, AATConfiguration configuration);

        [Static]
        [Export("reconfigure:")]
        void Reconfigure(AATConfiguration configuration);

        [Static]
        [Export("editConsent")]
        void EditConsent();

        [Static]
        [Export("reloadConsent")]
        void ReloadConsent();

        [Static]
        [Export("showConsentDialogIfNeeded")]
        void ShowConsentDialogIfNeeded();

        [Static]
        [Export("setManagedConsent:")]
        void SetManagedConsent(NSObject managedConsentValue);

        [Static]
        [Export("setManagedConsentNeedingUI:")]
        void SetManagedConsentNeedingUI(NSObject managedConsent);

        [Static]
		[Export("setViewController:")]
		void SetViewController(UIViewController viewController);

		[Static]
		[Export("setDebugEnabled:")]
		void SetDebugEnabled(bool enabled);

		[Static]
		[Export("setDebugShakeEnabled:")]
		void SetDebugShakeEnabled(bool enabled);

		[Static]
		[Export("getVersion")]
		string GetVersion();

		[Static]
		[Export("getDebugInfo")]
		string GetDebugInfo();

		[Static]
		[Export("isNetworkEnabled:")]
		bool IsNetworkEnabled(AATKitAdNetwork network);

		[Static]
		[Export("setNetwork:enabled:")]
		void SetNetworkEnabled(AATKitAdNetwork network, bool enabled);

		[Static]
		[Export("createPlacementWithName:andType:")]
		void CreatePlacement(string name, AATKitAdType type);

        [Static]
        [Export("createRewardedVideoPlacement:")]
        void CreateRewardedVideoPlacement(string name);

		[Static]
		[Export("startPlacementAutoReload:")]
		void StartPlacementAutoReload(NSObject placement);

		[Static]
		[Export("stopPlacementAutoReload:")]
		void StopPlacementAutoReload(NSObject placement);

		[Static]
		[Export("reloadPlacement:")]
		void ReloadPlacement(NSObject placement);

		[Static]
		[Export("reloadPlacement:forced:")]
		void ReloadPlacement(NSObject placement, bool forced);

		[Static]
		[Export("hasAdForPlacement:")]
		bool HasAdForPlacement(NSObject placement);

		[Static]
		[Export("getPlacementView:")]
		UIView GetPlacementView(NSObject placement);

		[Static]
		[Export("getPlacementName:")]
		string GetPlacementName(NSObject placement);

		[Static]
		[Export("getPlacementId:")]
		NSObject GetPlacementId(string name);

		[Static]
		[Export("showPlacement:")]
		bool ShowPlacement(NSObject placement);

		[Static]
		[Export("setTargetingInfo:")]
		void SetTargetingInfo(NSDictionary targetingInfo);

		[Static]
		[Export("setTargetingInfo:forPlacement:")]
		void SetTargetingInfo(NSDictionary targetingInfo, NSObject placement);

		[Static]
		[Export("addAdNetworkForKeywordTargeting:")]
		void AddAdNetworkForKeywordTargeting(AATKitAdNetwork adNetwork);

		[Static]
		[Export("removeAdNetworkForKeywordTargeting:")]
		void RemoveAdNetworkForKeywordTargeting(AATKitAdNetwork adNetwork);

	}
}
