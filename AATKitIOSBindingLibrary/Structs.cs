﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace AATKit
{
	[Native]
	public enum AATKitAdType : long
	{
		Banner320x53,
		Banner375x53,
		Banner414x53,
		Banner768x90,
		Banner300x250,
		Banner468x60,
		BannerMultiSize,
		Fullscreen,
		NativeAd,
		Banner480x32,
		Banner1024x66,
		Banner300x200,
		Banner90x80,
		Banner200x200
	}

	[Native]
	public enum AATKitAdNetwork : long
	{
        AATAdColony,
        AATAdMob,
        AATAddapptr,
        AATAdX,
        AATAmazon,
        AATAppLift, //__deprecated_enum_msg("AppLift is not supported anymore"),
        AATApplovin,
        AATAppnexus,
        AATAppnexusHB_DFP,
        AATAppnexusHB_Mopub,
        AATApprupt, //__deprecated_enum_msg("Apprupt is not supported anymore"),
        AATCriteo,
        AATDFP,
        AATFacebook,
        AATFlurry,
        AATHouse,
        AATHouseX,
        AATiAd, //__deprecated_enum_msg("iAd is not supported anymore"),
        AATInmobi,
        AATInneractive,
        AATLoopMe,
        AATMadvertise, //__deprecated_enum_msg("Madvertise is not supported anymore"),
        AATMdotM, //__deprecated_enum_msg("MdotM is not supported anymore"),
        AATMillennial, //__deprecated_enum_msg("Millennial is not supported anymore"),
        AATMobFox, //__deprecated_enum_msg("MobFox is not supported anymore"),
        AATMoPub,
        AATNexage,
        AATOneByAol,
        AATPermodo,
        AATPlayhaven, //__deprecated_enum_msg("Playhaven is not supported anymore"),
        AATPubMatic, //__deprecated_enum_msg("PubMatic is not supported anymore"),
        AATRevmob,
		AATRubicon,
        AATSmaato,
        AATSmartAd,
        AATSmartClip, //__deprecated_enum_msg("SmartClip is not supported anymore"),
        AATSmartStream, //__deprecated_enum_msg("SmartStream is not supported anymore"),
        AATUnity,
        AATVungle,
        AATUnknownNetwork
	}
}
