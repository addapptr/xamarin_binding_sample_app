//
//  SOMANativeAdAppWallLayouter.h
//  iSoma
//
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#import <UIKit/UIKit.h>
#import "SOMANativeAdLayouter.h"


@interface SOMANativeAdAppWallLayouter : NSObject<SOMANativeAdLayouter>

@end
