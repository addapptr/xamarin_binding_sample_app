//
//  SOMAWKWebView.h
//  iSoma
//
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface SOMAWKWebView : WKWebView
@property BOOL isTouched;
@property BOOL isTouchedOnced;
@end

