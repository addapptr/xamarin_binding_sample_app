//
//  SOMANativeAdLayouters.h
//  iSoma
//
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#ifndef SOMANativeAdLayouters_h
#define SOMANativeAdLayouters_h

#import "SOMANativeAdContentWallLayouter.h"
#import "SOMANativeAdAppWallLayouter.h"
#import "SOMANativeAdNewsFeedLayouter.h"
#import "SOMANativeAdContentStreamLayouter.h"
#import "SOMANativeAdChatListLayouter.h"
#import "SOMANativeAdCarouselLayouter.h"

#endif /* SOMANativeAdLayouters_h */
