//
//  SOMAMedRectAdView.h
//  iSoma
//
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#import "SOMAAdView.h"

/**
 * This Ad View shows ads in a medium rectangle.
 * The size of the ad has to be 300x250 
 */
@interface SOMAMedRectAdView : SOMAAdView

@end
