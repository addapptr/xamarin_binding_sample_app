//
//  SOMACloseButton.h
//  iSoma
//
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#import <UIKit/UIKit.h>

@interface SOMACloseButton : UIButton
@property BOOL onlyCross;
@property UIColor* crossColor;

@end
