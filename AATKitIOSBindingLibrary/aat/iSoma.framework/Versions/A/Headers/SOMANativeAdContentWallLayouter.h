//
//  SOMANativeAdContentWallLayouter.h
//  iSoma
//
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#import "SOMANativeAdLayouter.h"

@interface SOMANativeAdContentWallLayouter:NSObject<SOMANativeAdLayouter>

@end
