﻿using Xamarin.Forms;
using AATKit_Xamarin.iOS;
using AATKit;
using Foundation;

[assembly: Dependency(typeof(AATKitBinding))]
namespace AATKit_Xamarin.iOS
{
    public class AATKitManagedConsentListener : AATManagedConsentDelegate
    {
        private readonly AATKitBinding aatkitBinding;

        public AATKitManagedConsentListener(AATKitBinding aatkitBinding)
        {
            this.aatkitBinding = aatkitBinding;
        }

        public override void managedConsentNeedsUserInterface(NSObject managedConsent)
        {
            if (aatkitBinding != null)
            {
                aatkitBinding.ManagedConsentNeedsUserInterface((AATManagedConsent)managedConsent);
            }
        }

        public override void managedConsentCMPFinished()
        {
            if (aatkitBinding != null)
            {
                aatkitBinding.ManagedConsentCMPFinished();
            }
        }

        public override void managedConsentCMPFailedToLoad(NSObject managedConsent, string error)
        {
            if (aatkitBinding != null)
            {
                aatkitBinding.ManagedConsentCMPFailedToLoad(managedConsent, error);
            }
        }

        public override void managedConsentCMPFailedToShow(NSObject managedConsent, string error)
        {
            if (aatkitBinding != null)
            {
                aatkitBinding.ManagedConsentCMPFailedToShow(managedConsent, error);
            }
        }
    }
}
