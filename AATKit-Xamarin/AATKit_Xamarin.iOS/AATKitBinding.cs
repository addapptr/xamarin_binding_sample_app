﻿using System;
using Xamarin.Forms;
using AATKit_Xamarin.iOS;
using AATKit;
using UIKit;
using Foundation;
using CoreGraphics;
using ObjCRuntime;

[assembly: Dependency(typeof(AATKitBinding))]
namespace AATKit_Xamarin.iOS
{
	public class AATKitBinding : AATKitDelegate, IAATKitBinding
    {
		private static UIViewController viewController;
		private UIView multisizeBannerView;

		private static AATKitBinding instance;

		public static AATKitBinding Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new AATKitBinding();

					var window = UIApplication.SharedApplication.KeyWindow;
					viewController = window.RootViewController;
					while (viewController.PresentedViewController != null)
					{
						viewController = viewController.PresentedViewController;
					}
				}
				return instance;
			}
		}

		private bool isViewController()
		{
			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;
			while (vc.PresentedViewController != null)
			{
				vc = vc.PresentedViewController;
			}

			if (viewController == null || viewController != vc)
			{
				viewController = vc;
				if (viewController != null)
				{
					System.Diagnostics.Debug.WriteLine("[AATKitXamarin] Current ViewController has changed.");
					//AATKit.AATKitXamarin.SetViewController(viewController);
					return true;
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("[AATKitXamarin] Fatal error! ViewController is null.");
					return false;
				}
			}

			return true;
		}

		private void AlignBanner(UIView view, IAATKitBannerAlignment aligment)
		{
			CGSize controllerSize = viewController.View.Bounds.Size;
			CGRect frame = view.Frame;
            UIEdgeInsets safeAreaInsets = new UIEdgeInsets(0, 0, 0, 0);

            if(UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                safeAreaInsets = viewController.View.SafeAreaInsets;
            }

            switch (aligment)
			{
				case IAATKitBannerAlignment.TopLeft:
					{
                        frame.X = 0 + safeAreaInsets.Left;
                        frame.Y = 0 + safeAreaInsets.Top;
					}
					break;
				case IAATKitBannerAlignment.TopCenter:
					{
						frame.X = controllerSize.Width / 2 - view.Frame.Size.Width / 2;
                        frame.Y = 0 + safeAreaInsets.Top;
					}
					break;
				case IAATKitBannerAlignment.TopRight:
					{
                        frame.X = controllerSize.Width - view.Frame.Size.Width - safeAreaInsets.Right;
                        frame.Y = 0 + safeAreaInsets.Top;
					}
					break;
				case IAATKitBannerAlignment.BottomLeft:
					{
                        frame.X = 0 + safeAreaInsets.Left;
                        frame.Y = controllerSize.Height - view.Frame.Size.Height - safeAreaInsets.Bottom;
					}
					break;
				case IAATKitBannerAlignment.BottomCenter:
					{
						frame.X = controllerSize.Width / 2 - view.Frame.Size.Width / 2;
                        frame.Y = controllerSize.Height - view.Frame.Size.Height - safeAreaInsets.Bottom;
					}
					break;
				case IAATKitBannerAlignment.BottomRight:
					{
                        frame.X = controllerSize.Width - view.Frame.Size.Width - safeAreaInsets.Right;
                        frame.Y = controllerSize.Height - view.Frame.Size.Height - safeAreaInsets.Bottom;
					}
					break;
			}
			view.Frame = frame;
		}

        private bool IsIphoneX()
        {
            bool isIphoneX = false;
            nfloat height = UIScreen.MainScreen.Bounds.Height * UIScreen.MainScreen.Scale;
            nfloat width = UIScreen.MainScreen.Bounds.Width * UIScreen.MainScreen.Scale;
            int maxValue = (int)Math.Max(height, width);

            if (maxValue == 2436)
            {
                isIphoneX = true;
            }

            return isIphoneX;
        }

        private bool IsScreenOrientationPortait()
        {
            var currentOrientation = UIApplication.SharedApplication.StatusBarOrientation;
            bool isPortrait = currentOrientation == UIInterfaceOrientation.Portrait
                || currentOrientation == UIInterfaceOrientation.PortraitUpsideDown;

            return isPortrait;
        }

		private AATKit.AATKitAdNetwork getIOSAdNetwork(IAATKitAdNetwork network)
		{
			AATKit.AATKitAdNetwork adNetwork;
			switch (network)
			{
                case IAATKitAdNetwork.AdColony:
                    return AATKit.AATKitAdNetwork.AATAdColony;
                case IAATKitAdNetwork.AdMob:
                    return AATKit.AATKitAdNetwork.AATAdMob;
                case IAATKitAdNetwork.AdX:
                    return AATKit.AATKitAdNetwork.AATAdX;
                case IAATKitAdNetwork.Applovin:
                    return AATKit.AATKitAdNetwork.AATApplovin;
                case IAATKitAdNetwork.AppNexus:
                    return AATKit.AATKitAdNetwork.AATAppnexus;
                case IAATKitAdNetwork.Criteo:
                    return AATKit.AATKitAdNetwork.AATCriteo;
                case IAATKitAdNetwork.DFP:
                    return AATKit.AATKitAdNetwork.AATDFP;
                case IAATKitAdNetwork.UnknownNetwork:
                    return AATKit.AATKitAdNetwork.AATUnknownNetwork;
                case IAATKitAdNetwork.Facebook:
                    return AATKit.AATKitAdNetwork.AATFacebook;
                case IAATKitAdNetwork.Flurry:
                    return AATKit.AATKitAdNetwork.AATFlurry;
                case IAATKitAdNetwork.MoPub:
                    return AATKit.AATKitAdNetwork.AATMoPub;
                case IAATKitAdNetwork.OneByAOL:
                    return AATKit.AATKitAdNetwork.AATOneByAol;
                case IAATKitAdNetwork.Rubicon:
                    return AATKit.AATKitAdNetwork.AATRubicon;
                case IAATKitAdNetwork.Smaato:
                    return AATKit.AATKitAdNetwork.AATSmaato;
                case IAATKitAdNetwork.SmartAd:
                    return AATKit.AATKitAdNetwork.AATSmartAd;
                case IAATKitAdNetwork.Unity:
                    return AATKit.AATKitAdNetwork.AATUnity;
				default:
                    return AATKit.AATKitAdNetwork.AATUnknownNetwork;
			}
		}

		//--------------------------------------------------------------------------------------------------------------

        public void InitAATKit(AATKitConfiguration configuration)
		{
			if (isViewController())
            {
                AATKit.AATConfiguration nativeConfiguration = new AATConfiguration();
                nativeConfiguration.alternativeBundleID = configuration.alternativeBundleID;
                if (configuration.initialRules.Length == 0)
                {
                    nativeConfiguration.initialRules = new NSArray();
                }
                else
                {
                    nativeConfiguration.initialRules = NSArray.FromObjects(configuration.initialRules.Split('\n'));
                }
                nativeConfiguration.shouldCacheRules = configuration.shouldCacheRules;
                nativeConfiguration.shouldReportMetricsUsingAlternativeBundleID = configuration.shouldReportMetricsUsingAlternativeBundleID;
                nativeConfiguration.testModeAccountID = configuration.testModeAccountID;
                SetRuntimeConfiguration(configuration, nativeConfiguration);
                AATKit.AATKitXamarin.Init(viewController, this, nativeConfiguration);
			}
        }

        public void Reconfigure(AATKitConfiguration configuration)
        {
            AATKit.AATConfiguration nativeConfiguration = new AATConfiguration();
            SetRuntimeConfiguration(configuration, nativeConfiguration);
            AATKit.AATKitXamarin.Reconfigure(nativeConfiguration);
        }

        private void SetRuntimeConfiguration(AATKitConfiguration configuration, AATConfiguration nativeConfiguration)
        {
            nativeConfiguration.consentRequired = configuration.consentRequired;
            nativeConfiguration.shouldUseGeoLocation = configuration.useGeoLocation;

			if (configuration.consent != null)
            {
                SetConsent(configuration.consent, nativeConfiguration);
            }
        }

        private void SetConsent(AATKitConsent consent, AATConfiguration nativeConfiguration)
        {
			if (consent is AATKitManagedConsent)
            {
                SetManagedConsent(consent, nativeConfiguration);
            }
            else if (consent is AATKitSimpleConsent)
            {
                SetSimpleConsent(consent, nativeConfiguration);
            }
            else
            {
                throw new ArgumentException("Consent type: " + consent.GetType() + " is not supported.");
            }
        }

        private static void SetSimpleConsent(AATKitConsent consent, AATConfiguration nativeConfiguration)
        {
            AATKitSimpleConsent simpleConsent = consent as AATKitSimpleConsent;
            AATSimpleConsent ntiveConsent = new AATSimpleConsent();
			ntiveConsent.initWithNonIABConsent((int)simpleConsent.nonIABConsent);
            nativeConfiguration.consent = ntiveConsent;
        }

        private void SetManagedConsent(AATKitConsent consent, AATConfiguration nativeConfiguration)
        {
            AATKitManagedConsent managedConsent = consent as AATKitManagedConsent;
			AATKitCmp cmp = managedConsent.cmp;
			AATManagedConsent nativeConsent = null;

			if (cmp is AATKitCmpGoogle)
            {
				nativeConsent = new AATManagedConsent();
				AATKitManagedConsentListener consentDelegate = new AATKitManagedConsentListener(this);
				nativeConsent.initWithDelegate(consentDelegate, new AATCMPGoogle());
			}
			else if(cmp is AATKitCmpOgury)
            {
				nativeConsent = new AATManagedConsent();
				AATCMPOgury cmpOgury = new AATCMPOgury();
				cmpOgury.initWithAssetKey((cmp as AATKitCmpOgury).assetKey);
				AATKitManagedConsentListener consentDelegate = new AATKitManagedConsentListener(this);
				nativeConsent.initWithDelegate(consentDelegate, cmpOgury);
			}

			nativeConfiguration.consent = nativeConsent;
			if(nativeConsent != null)
            {
				AATKit.AATKitXamarin.SetManagedConsent(nativeConsent);
            }
		}

		public void EditConsent()
		{
			if (isViewController())
			{
				AATKit.AATKitXamarin.EditConsent();
			}
		}

		public void ReloadConsent()
		{
			if (isViewController())
			{
				AATKit.AATKitXamarin.ReloadConsent();
			}
		}

        public void ShowConsentDialogIfNeeded()
        {
            if (isViewController())
            {
                AATKit.AATKitXamarin.ShowConsentDialogIfNeeded();
            }
        }

        public void SetDebugEnabled(bool enabled)
		{
			if (isViewController())
			{
				AATKit.AATKitXamarin.SetDebugEnabled(enabled);
			}
		}

		public void SetDebugShakeEnabled(bool enabled)
		{
			if (isViewController())
			{
				AATKit.AATKitXamarin.SetDebugShakeEnabled(enabled);
			}
		}

		public string GetVersion()
		{
			return AATKit.AATKitXamarin.GetVersion();
		}

		public string GetDebugInfo()
		{
			return AATKit.AATKitXamarin.GetDebugInfo();
		}

		public bool IsNetworkEnabled(IAATKitAdNetwork network)
		{
			if (isViewController())
			{
				return AATKit.AATKitXamarin.IsNetworkEnabled(getIOSAdNetwork(network));
			}
			else
			{
				return false;
			}
		}

		public void SetNetworkEnabled(IAATKitAdNetwork network, bool enabled)
		{
			if (isViewController())
			{
				AATKit.AATKitXamarin.SetNetworkEnabled(getIOSAdNetwork(network), enabled);
			}
		}

		public void CreatePlacement(string name, IAATKitAdType type)
		{
			if (isViewController())
			{
                if (type == IAATKitAdType.Rewarded)
                {
                    AATKit.AATKitXamarin.CreateRewardedVideoPlacement(name);
                }
                else
                {
                    AATKit.AATKitAdType adType = AATKit.AATKitAdType.Banner320x53;
                    switch (type)
                    {
                        case IAATKitAdType.Banner320x53:
                            adType = AATKit.AATKitAdType.Banner320x53;
                            break;
                        case IAATKitAdType.Banner768x90:
                            adType = AATKit.AATKitAdType.Banner768x90;
                            break;
                        case IAATKitAdType.Banner300x250:
                            adType = AATKit.AATKitAdType.Banner300x250;
                            break;
                        case IAATKitAdType.MultiSizeBanner:
                            adType = AATKit.AATKitAdType.BannerMultiSize;
                            break;
                        case IAATKitAdType.Fullscreen:
                            adType = AATKit.AATKitAdType.Fullscreen;
                            break;
                    }

                    AATKit.AATKitXamarin.CreatePlacement(name, adType);
                }
			}
		}

		public void AddPlacementToView(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				UIView view = AATKit.AATKitXamarin.GetPlacementView(placement);
				viewController.View.AddSubview(view);
				viewController.View.BringSubviewToFront(view);
			}
		}

		public void RemovePlacementFromView(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				UIView view = AATKit.AATKitXamarin.GetPlacementView(placement);
				view.RemoveFromSuperview();
			}
		}

		public void ReloadPlacement(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				AATKit.AATKitXamarin.ReloadPlacement(placement);
			}
		}

		public void ReloadPlacementForced(string name, bool force)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				AATKit.AATKitXamarin.ReloadPlacement(placement, force);
			}
		}

		public void StartPlacementAutoReload(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				AATKit.AATKitXamarin.StartPlacementAutoReload(placement);
			}
		}

		public void StopPlacementAutoReload(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				AATKit.AATKitXamarin.StopPlacementAutoReload(placement);
			}
		}

		public bool hasAdForPlacement(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				return AATKit.AATKitXamarin.HasAdForPlacement(placement);
			}
			else
			{
				return false;
			}	
		}

		public void SetPlacementAlignment(string name, IAATKitBannerAlignment aligment)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				UIView view = AATKit.AATKitXamarin.GetPlacementView(placement);
				AlignBanner(view, aligment);
			}
		}

        public void SetPlacementAlignmentWithOffset(string name, IAATKitBannerAlignment alignment, int x, int y)
        {
            if (isViewController())
            {
                NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
                UIView view = AATKit.AATKitXamarin.GetPlacementView(placement);
                AlignBanner(view, alignment);
                SetOffset(x, y, view);
            }
        }

        private static void SetOffset(int x, int y, UIView view)
        {
            CGRect frame = view.Frame;
            frame.Offset(x, y);
            view.Frame = frame;
        }

        public void SetPlacementPosition(string name, int x, int y)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				UIView view = AATKit.AATKitXamarin.GetPlacementView(placement);
				CGRect frame = view.Frame;
				frame.X = x;
				frame.Y = y;
				view.Frame = frame;
			}
		}

		public void ShowPlacement(string name)
		{
			if (isViewController())
			{
				NSObject placement = AATKit.AATKitXamarin.GetPlacementId(name);
				AATKit.AATKitXamarin.ShowPlacement(placement);
			}
		}

		// AATKit Events
		public event EventHandler<AATKitEventArgs> AATKitHaveAdEvent;
		public event EventHandler<AATKitEventArgs> AATKitHaveAdOnMultiSizeBannerEvent;
		public event EventHandler<AATKitEventArgs> AATKitNoAdsEvent;
		public event EventHandler<AATKitEventArgs> AATKitShowingEmptyEvent;
		public event EventHandler<AATKitEventArgs> AATKitPauseForAdEvent;
		public event EventHandler<AATKitEventArgs> AATKitResumeAfterAdEvent;
		public event EventHandler<AATKitEventArgs> AATKitEarnedIncentiveEvent;
		public event EventHandler<AATKitEventArgs> AATKitObtainedAdRulesEvent;
        public event EventHandler<AATKitEventArgs> AATKitManagedConsentNeedsUserInterface;
		public event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFinished;
		public event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFailedToLoad;
		public event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFailedToShow;


		public override void AATKitHaveAd(NSObject placement)
		{
			if (AATKitHaveAdEvent != null)
			{
				AATKitHaveAdEvent(this, new AATKitEventArgs(AATKit.AATKitXamarin.GetPlacementName(placement)));
			}
		}

		public override void AATKitHaveAdOnMultiSizeBanner(NSObject placement, UIView placementView)
		{
			if (isViewController())
			{
				if (multisizeBannerView != null)
				{
					multisizeBannerView.RemoveFromSuperview();
					multisizeBannerView = null;
				}
				multisizeBannerView = placementView;
				viewController.View.AddSubview(multisizeBannerView);
				viewController.View.BringSubviewToFront(multisizeBannerView);
			}

			if (AATKitHaveAdOnMultiSizeBannerEvent != null)
			{
				AATKitHaveAdOnMultiSizeBannerEvent(this, new AATKitEventArgs(AATKit.AATKitXamarin.GetPlacementName(placement)));
			}
		}

		public override void AATKitNoAds(NSObject placement)
		{
			if (AATKitNoAdsEvent != null)
			{
				AATKitNoAdsEvent(this, new AATKitEventArgs(AATKit.AATKitXamarin.GetPlacementName(placement)));
			}
		}

		public override void AATKitShowingEmpty(NSObject placement)
		{
			if (AATKitShowingEmptyEvent != null)
			{
				AATKitShowingEmptyEvent(this, new AATKitEventArgs(AATKit.AATKitXamarin.GetPlacementName(placement)));
			}
		}

		public override void AATKitPauseForAd()
		{
			if (AATKitPauseForAdEvent != null)
			{
				AATKitPauseForAdEvent(this, new AATKitEventArgs("Unknown"));
			}
		}

		public override void AATKitResumeAfterAd()
		{
			if (AATKitResumeAfterAdEvent != null)
			{
				AATKitResumeAfterAdEvent(this, new AATKitEventArgs("Unknown"));
			}
		}

        public void ManagedConsentNeedsUserInterface(AATManagedConsent managedConsent)
        {
            if (isViewController())
            {
                AATKit.AATKitXamarin.SetManagedConsentNeedingUI(managedConsent);
            }

            if (AATKitManagedConsentNeedsUserInterface != null)
            {
                AATKitManagedConsentNeedsUserInterface(this, null);
            }
        }

		public void ManagedConsentCMPFinished()
		{
			if (AATKitManagedConsentCMPFinished != null)
			{
				AATKitManagedConsentCMPFinished(this, null);
			}
		}

		public void ManagedConsentCMPFailedToLoad(NSObject managedConsent, string error)
		{
			if (AATKitManagedConsentCMPFailedToLoad != null)
			{
				AATKitManagedConsentCMPFailedToLoad(this, new AATKitEventArgs(error));
			}
		}

		public void ManagedConsentCMPFailedToShow(NSObject managedConsent, string error)
		{
			if (AATKitManagedConsentCMPFailedToShow != null)
			{
				AATKitManagedConsentCMPFailedToShow(this, new AATKitEventArgs(error));
			}
		}
    }
}
