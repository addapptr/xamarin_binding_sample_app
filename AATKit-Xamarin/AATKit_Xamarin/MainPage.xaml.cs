﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AATKit_Xamarin
{
    public partial class MainPage : ContentPage
    {
		private const string BANNER_PLACEMENT = "bannerPlacement";
		private const string MULTISIZE_BANNER_PLACEMENT = "multisizeBanner";
		private const string FULLSCREEN_PLACEMENT = "fullscreenPlacement";

		private IAATKitBinding binding;
		private IAATKitBannerAlignment bannerAlignment = IAATKitBannerAlignment.BottomCenter;

        public MainPage()
        {
            InitializeComponent();
			binding = DependencyService.Get<IAATKitBinding>();
			binding.AATKitHaveAdEvent += AATKitHaveAdEvent;
            binding.AATKitManagedConsentNeedsUserInterface += AATKitManagedConsentNeedsUserInterface;
			binding.AATKitManagedConsentCMPFailedToLoad += AATKitManagedConsentCMPFailedToLoad;
			binding.AATKitManagedConsentCMPFinished += AATKitManagedConsentCMPFinished;
			binding.AATKitManagedConsentCMPFailedToShow += AATKitManagedConsentCMPFailedToShow;
		}

		void InitAATKit(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.SetDebugEnabled(true);
                AATKitConfiguration configuration = new AATKitConfiguration();
                configuration.testModeAccountID = 74;
                configuration.consentRequired = true;

				configuration.consent = new AATKitManagedConsent()
				{
					cmp = new AATKitCmpGoogle()
                };
                binding.InitAATKit(configuration);
                binding.SetDebugShakeEnabled(true);
				string version = binding.GetVersion();
				VersionLabel.Text = version;

				System.Diagnostics.Debug.WriteLine("AATKIT DEBUG INFO:\n"+binding.GetDebugInfo());
            }
		}

		void CreateBanner(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.CreatePlacement(BANNER_PLACEMENT, IAATKitAdType.Banner320x53);
				binding.AddPlacementToView(BANNER_PLACEMENT);
			}
		}

		void CreateMultisizeBanner(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.CreatePlacement(MULTISIZE_BANNER_PLACEMENT, IAATKitAdType.MultiSizeBanner);
				//binding.AddPlacementToView(MULTISIZE_BANNER_PLACEMENT);
			}

			System.Diagnostics.Debug.WriteLine("AATKIT DEBUG INFO:\n"+binding.GetDebugInfo());
		}

		void EnableBannerAutoreload(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.StartPlacementAutoReload(BANNER_PLACEMENT);
			}
		}

		void EnableMultisizeBannerAutoreload(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.StartPlacementAutoReload(MULTISIZE_BANNER_PLACEMENT);
			}
		}

		void ChangeBannerAlignment(object sender, EventArgs e)
		{
			if (binding != null)
			{
                switch (bannerAlignment)
                {
                    case IAATKitBannerAlignment.BottomCenter:
                        binding.SetPlacementAlignmentWithOffset(BANNER_PLACEMENT, IAATKitBannerAlignment.BottomRight, -10, -10);
                        bannerAlignment = IAATKitBannerAlignment.BottomRight;
                        break;
                    case IAATKitBannerAlignment.BottomRight:
                        binding.SetPlacementAlignmentWithOffset(BANNER_PLACEMENT, IAATKitBannerAlignment.BottomLeft, 10, -10);
                        bannerAlignment = IAATKitBannerAlignment.BottomLeft;
                        break;
                    case IAATKitBannerAlignment.BottomLeft:
                        binding.SetPlacementAlignmentWithOffset(BANNER_PLACEMENT, IAATKitBannerAlignment.TopCenter, 0, 10);
                        bannerAlignment = IAATKitBannerAlignment.TopCenter;
                        break;
                    case IAATKitBannerAlignment.TopCenter:
                        binding.SetPlacementAlignmentWithOffset(BANNER_PLACEMENT, IAATKitBannerAlignment.TopRight, -10, 10);
                        bannerAlignment = IAATKitBannerAlignment.TopRight;
                        break;
                    case IAATKitBannerAlignment.TopRight:
                        binding.SetPlacementAlignmentWithOffset(BANNER_PLACEMENT, IAATKitBannerAlignment.TopLeft, 10, 10);
                        bannerAlignment = IAATKitBannerAlignment.TopLeft;
                        break;
                    case IAATKitBannerAlignment.TopLeft:
                        binding.SetPlacementAlignmentWithOffset(BANNER_PLACEMENT, IAATKitBannerAlignment.BottomCenter, 0, -10);
                        bannerAlignment = IAATKitBannerAlignment.BottomCenter;
                        break;
                }
            }
		}

		void CreateInterstitial(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.CreatePlacement(FULLSCREEN_PLACEMENT, IAATKitAdType.Fullscreen);
			}
		}

		void EnableInterstitialAutoreload(object sender, EventArgs e)
		{
			if (binding != null)
			{
				binding.StartPlacementAutoReload(FULLSCREEN_PLACEMENT);
			}
		}

		void ShowInterstitial(object sender, EventArgs e)
		{
			if (binding != null)
			{
                binding.ShowPlacement(FULLSCREEN_PLACEMENT);
            }
        }

		// AATKit Events
		public void AATKitHaveAdEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitHaveAd: " + e.name);
		}

		public void AATKitHaveAdOnMultiSizeBannerEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitHaveAdOnMultiSizeBannerEvent: " + e.name);
		}

		public void AATKitNoAdsEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitNoAds: " + e.name);
		}

		public void AATKitShowingEmptyEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitShowingEmpty: " + e.name);
		}

		public void AATKitPauseForAdEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitPauseForAd: " + e.name);
		}

		public void AATKitResumeAfterAdEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitResumeAfterAd: " + e.name);
		}

		public void AATKitEarnedIncentiveEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitEarnedIncentiveEvent: " + e.name);
		}

		public void AATKitObtainedAdRulesEvent(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitObtainedAdRulesEvent: " + e.name);
		}
        
        public void AATKitManagedConsentNeedsUserInterface(object sender, AATKitEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AATKitManagedConsentNeedsUserInterface");
            binding.ShowConsentDialogIfNeeded();
        }

		public void AATKitManagedConsentCMPFailedToLoad(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitManagedConsentCMPFailedToLoad");
		}

		public void AATKitManagedConsentCMPFinished(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitManagedConsentCMPFinished");
		}

		public void AATKitManagedConsentCMPFailedToShow(object sender, AATKitEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("AATKitManagedConsentCMPFailedToShow");
		}
	}
}
