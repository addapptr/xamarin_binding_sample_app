﻿using System;

namespace AATKit_Xamarin
{
	public enum IAATKitAdType : int
	{
		Banner320x53,
		Banner768x90,
		Banner300x250,
		Fullscreen,
		Banner468x60,
		Native,
		MultiSizeBanner,
        Rewarded
	}

	public enum IAATKitBannerAlignment : int
	{
		TopLeft,
		TopCenter,
		TopRight,
		BottomLeft,
		BottomCenter,
		BottomRight,
	}

    public enum IAATKitAdNetwork : int
    {
        Inmobi,
        AdMob,
        Empty,
        Applovin,
        SmartAd,
        AdX,
        DFP,
        Smaato,
        Facebook,
        Unity,
        AdColony,
        MoPub,
        Flurry,
        AppNexus,
        Ogury,
        Criteo,
        CriteoSDK,
        Spotx,
        OneByAOL,
        Rubicon,
        Yandex,
        AmazonHB,
        InLoco,
        Teads,
        Genericvast,
        UnknownNetwork
    }

    public interface IAATKitBinding
    {
        void InitAATKit(AATKitConfiguration configuration);
        void Reconfigure(AATKitConfiguration configuration);
        void EditConsent();
        void ReloadConsent();
        void ShowConsentDialogIfNeeded();
        void SetDebugEnabled(bool enabled);
		void SetDebugShakeEnabled(bool enabled);
		string GetVersion();
		string GetDebugInfo();

		bool IsNetworkEnabled(IAATKitAdNetwork network);
		void SetNetworkEnabled(IAATKitAdNetwork network, bool enabled);

		void CreatePlacement(string name, IAATKitAdType type);
		void AddPlacementToView(string name);
		void RemovePlacementFromView(string name);
		void ReloadPlacement(string name);
		void ReloadPlacementForced(string name, bool force);
		void StartPlacementAutoReload(string name);
		void StopPlacementAutoReload(string name);
		bool hasAdForPlacement(string name);

		void SetPlacementAlignment(string name, IAATKitBannerAlignment alignment);
        void SetPlacementAlignmentWithOffset(string name, IAATKitBannerAlignment alignment, int x, int y);
        void SetPlacementPosition(string name, int x, int y);
		void ShowPlacement(string name);

		// AATKit Events
		event EventHandler<AATKitEventArgs> AATKitHaveAdEvent;
		event EventHandler<AATKitEventArgs> AATKitHaveAdOnMultiSizeBannerEvent;
		event EventHandler<AATKitEventArgs> AATKitNoAdsEvent;
		event EventHandler<AATKitEventArgs> AATKitShowingEmptyEvent;
		event EventHandler<AATKitEventArgs> AATKitPauseForAdEvent;
		event EventHandler<AATKitEventArgs> AATKitResumeAfterAdEvent;
		event EventHandler<AATKitEventArgs> AATKitEarnedIncentiveEvent;
		event EventHandler<AATKitEventArgs> AATKitObtainedAdRulesEvent;
        event EventHandler<AATKitEventArgs> AATKitManagedConsentNeedsUserInterface;
        event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFinished;
        event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFailedToLoad;
        event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFailedToShow;
    }

    public class AATKitEventArgs : EventArgs
    {
        public string name { get; protected set; }

        public AATKitEventArgs(string name)
        {
            this.name = name;
        }
    }

    public abstract class AATKitConsent
    {

    }

    public class AATKitSimpleConsent : AATKitConsent
    {
        public enum Consent : int
        {
            UNKNOWN,
            OBTAINED,
            WITHHELD,
        }

        public Consent nonIABConsent = Consent.UNKNOWN;
    }

    public abstract class AATKitCmp
    {
    }

    public class AATKitCmpGoogle : AATKitCmp
    {

    }

    public class AATKitCmpOgury : AATKitCmp
    {
        public String assetKey;
    }

    public class AATKitManagedConsent : AATKitConsent
    {
        public AATKitCmp cmp;
    }

    public class AATKitConfiguration
    {
        public string alternativeBundleID { get; set; }
        public string initialRules { get; set; }
        public bool shouldCacheRules { get; set; }
        public bool shouldReportMetricsUsingAlternativeBundleID { get; set; }
        public int testModeAccountID { get; set; }
        public bool consentRequired { get; set; }
        public bool useGeoLocation { get; set; }
        public AATKitConsent consent { get; set; }

        public AATKitConfiguration()
        {
            this.alternativeBundleID = string.Empty;
            this.initialRules = string.Empty;
            this.shouldCacheRules = true;
            this.shouldReportMetricsUsingAlternativeBundleID = true;
            this.testModeAccountID = 0;
            this.consentRequired = true;
            this.useGeoLocation = false;
            this.consent = null;
        }
    }
}