using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using AATKit_Xamarin.Droid;
using Com.Addapptr.Aatkit_xamarin_wrapper;

[assembly: Dependency(typeof(AATKitBinding))]
namespace AATKit_Xamarin.Droid
{
    class AATKitBinding : Java.Lang.Object, IAATKitBinding, AATKitWrapper.IDelegate
    {
        private static AATKitBinding instance;
		private static AATKitWrapper wrapper;
		private static Activity activity;
		private Com.Addapptr.Aatkit_xamarin_wrapper.MultiSizeBanner multiSizeBanner;
        private static Android.App.Application androidApplication;

        public static AATKitBinding Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AATKitBinding();
					wrapper = new AATKitWrapper();
                }
                return instance;
            }
        }

		private Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork getAndroidAdNetwork(IAATKitAdNetwork network)
		{
			switch (network)
			{
                case IAATKitAdNetwork.AdColony:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Adcolony;
                case IAATKitAdNetwork.AdMob:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Admob;
                case IAATKitAdNetwork.AdX:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Adx;
                case IAATKitAdNetwork.Applovin:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Applovin;
                case IAATKitAdNetwork.AppNexus:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Appnexus;
                case IAATKitAdNetwork.Criteo:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Criteo;
                case IAATKitAdNetwork.CriteoSDK:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Criteosdk;
                case IAATKitAdNetwork.DFP:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Dfp;
                case IAATKitAdNetwork.UnknownNetwork:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Empty;
                case IAATKitAdNetwork.Facebook:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Facebook;
                case IAATKitAdNetwork.Flurry:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Flurry;
                case IAATKitAdNetwork.Genericvast:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Genericvast;
                case IAATKitAdNetwork.Inmobi:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Inmobi;
                case IAATKitAdNetwork.MoPub:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Mopub;
                case IAATKitAdNetwork.Ogury:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Ogury;
                case IAATKitAdNetwork.OneByAOL:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Onebyaol;
                case IAATKitAdNetwork.Rubicon:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Rubicon;
                case IAATKitAdNetwork.Smaato:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Smaato;
                case IAATKitAdNetwork.SmartAd:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Smartad;
                case IAATKitAdNetwork.Spotx:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Spotx;
                case IAATKitAdNetwork.Unity:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Unityads;
                case IAATKitAdNetwork.Empty:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Empty;
                case IAATKitAdNetwork.AmazonHB:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Amazonhb;
                case IAATKitAdNetwork.Yandex:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Yandex;
                case IAATKitAdNetwork.InLoco:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Inloco;
                case IAATKitAdNetwork.Teads:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Teads; 
                default:
                    return Com.Addapptr.Aatkit_xamarin_wrapper.AdNetwork.Empty;
			}
		}

		internal void Initialize(Android.App.Application application)
		{
            androidApplication = application;
		}

		internal void OnResume(MainActivity mainActivity)
		{
			if (mainActivity != null)
			{
				activity = mainActivity;
				wrapper.OnActivityResume(mainActivity);
			}
		}

        internal void OnPause(MainActivity mainActivity)
		{
			if (mainActivity != null)
			{
				activity = mainActivity;
				wrapper.OnActivityPause(mainActivity);
			}
		}

        public void InitAATKit(AATKitConfiguration configuration)
        {
            Com.Addapptr.Aatkit_xamarin_wrapper.AATConfiguration nativeConfiguration = new AATConfiguration();
            nativeConfiguration.AlternativeBundleID = configuration.alternativeBundleID;
            nativeConfiguration.InitialRules = configuration.initialRules;
            nativeConfiguration.ShouldCacheRules = (Java.Lang.Boolean)configuration.shouldCacheRules;
            nativeConfiguration.ShouldReportMetricsUsingAlternativeBundleID = (Java.Lang.Boolean)configuration.shouldReportMetricsUsingAlternativeBundleID;
            nativeConfiguration.TestModeAccountID = configuration.testModeAccountID;
            SetRuntimeConfiguration(configuration, nativeConfiguration);

            wrapper.Init(androidApplication, this, nativeConfiguration);
        }
        
        public void Reconfigure(AATKitConfiguration configuration)
        {
            Com.Addapptr.Aatkit_xamarin_wrapper.AATConfiguration nativeConfiguration = new AATConfiguration();
            SetRuntimeConfiguration(configuration, nativeConfiguration);
            wrapper.Reconfigure(nativeConfiguration);
        }

        private void SetRuntimeConfiguration(AATKitConfiguration configuration, AATConfiguration nativeConfiguration)
        {
            nativeConfiguration.ConsentRequired = (Java.Lang.Boolean)configuration.consentRequired;
            nativeConfiguration.UseGeoLocation = configuration.useGeoLocation;

            if(configuration.consent != null)
            {
                SetConsent(configuration.consent, nativeConfiguration);
            }
        }

        private void SetConsent(AATKitConsent consent, AATConfiguration nativeConfiguration)
        {
            if (consent is AATKitManagedConsent)
            {
                SetManagedConsent(consent, nativeConfiguration);
            }
            else if (consent is AATKitSimpleConsent)
            {
                SetSimpleConsent(consent, nativeConfiguration);
            }
            else
            {
                throw new ArgumentException("Consent type: " + consent.GetType() + " is not supported.");
            }
        }

        private static void SetManagedConsent(AATKitConsent consent, AATConfiguration nativeConfiguration)
        {
            AATKitManagedConsent managedConsent = consent as AATKitManagedConsent;

            AATCmp cmp = null;
            if(managedConsent.cmp is AATKitCmpGoogle)
            {
                cmp = new AATCmpGoogle();
            }
            else if(managedConsent.cmp is AATKitCmpOgury)
            {
                cmp = new AATCmpOgury()
                {
                    AssetKey = (managedConsent.cmp as AATKitCmpOgury).assetKey
                };
            }

            AATManagedConsent nativeManagedConsent = new AATManagedConsent()
            {
                Cmp = cmp
            };
            nativeConfiguration.Consent = nativeManagedConsent;
        }

        private static void SetSimpleConsent(AATKitConsent consent, AATConfiguration nativeConfiguration)
        {
            AATKitSimpleConsent simpleConsent = consent as AATKitSimpleConsent;
            AATSimpleConsent nativeSimpleConsent = new AATSimpleConsent();
            switch (simpleConsent.nonIABConsent)
            {
                case AATKitSimpleConsent.Consent.UNKNOWN:
                    nativeSimpleConsent.NonIABConsent = AATSimpleConsent.Consent.Unknown;
                    break;
                case AATKitSimpleConsent.Consent.OBTAINED:
                    nativeSimpleConsent.NonIABConsent = AATSimpleConsent.Consent.Obtained;
                    break;
                case AATKitSimpleConsent.Consent.WITHHELD:
                    nativeSimpleConsent.NonIABConsent = AATSimpleConsent.Consent.Withheld;
                    break;
            }
            nativeConfiguration.Consent = nativeSimpleConsent;
        }

        public void EditConsent()
        {
            wrapper.EditConsent();
        }

        public void ReloadConsent()
        {
            wrapper.ReloadConsent();
        }

        public void ShowConsentDialogIfNeeded()
        {
            wrapper.ShowConsentDialogIfNeeded();
        }

        public void SetDebugEnabled(bool enabled)
		{
			wrapper.SetDebugEnabled(enabled);
		}

		public void SetDebugShakeEnabled(bool enabled)
		{
			wrapper.SetDebugShakeEnabled(enabled);
		}

		public string GetVersion()
		{
			return wrapper.AatkitVersion();
		}

		public string GetDebugInfo()
		{
			return wrapper.AatkitDebugInfo();
		}

		public bool IsNetworkEnabled(IAATKitAdNetwork network)
		{
			return wrapper.IsNetworkEnabled(getAndroidAdNetwork(network));
		}

		public void SetNetworkEnabled(IAATKitAdNetwork network, bool enabled)
		{
			wrapper.SetNetworkEnabled(getAndroidAdNetwork(network), enabled);
		}

		public void CreatePlacement(string name, IAATKitAdType type)
		{
            if (type == IAATKitAdType.Rewarded)
            {
                wrapper.CreateRewardedVideoPlacement(name);
            }
            else
            {
                Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize adType = null;
                switch (type)
                {
                    case IAATKitAdType.Banner320x53:
                        adType = Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize.Banner320x53;
                        break;
                    case IAATKitAdType.Banner768x90:
                        adType = Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize.Banner768x90;
                        break;
                    case IAATKitAdType.Banner300x250:
                        adType = Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize.Banner300x250;
                        break;
                    case IAATKitAdType.Fullscreen:
                        adType = Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize.Fullscreen;
                        break;
                    case IAATKitAdType.Banner468x60:
                        adType = Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize.Banner468x60;
                        break;
                    case IAATKitAdType.MultiSizeBanner:
                        adType = Com.Addapptr.Aatkit_xamarin_wrapper.PlacementSize.MultiSizeBanner;
                        break;
                }

                wrapper.CreatePlacement(name, adType);
            }
		}

		public void AddPlacementToView(string name)
		{
			FrameLayout mainLayout = (FrameLayout)activity.Window.DecorView.FindViewById(Android.Resource.Id.Content);
			Android.Views.View placementView = wrapper.GetPlacementView(wrapper.GetPlacementId(name));
			FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
			layoutParams.Gravity = Android.Views.GravityFlags.CenterHorizontal | Android.Views.GravityFlags.Bottom;
			mainLayout.AddView(placementView, layoutParams);
		}

		public void RemovePlacementFromView(string name)
		{
			FrameLayout mainLayout = (FrameLayout)activity.Window.DecorView.FindViewById(Android.Resource.Id.Content);
			Android.Views.View placementView = wrapper.GetPlacementView(wrapper.GetPlacementId(name));
			mainLayout.RemoveView(placementView);
		}

		public void ReloadPlacement(string name)
		{
			wrapper.ReloadPlacement(wrapper.GetPlacementId(name));
		}

		public void ReloadPlacementForced(string name, bool force)
		{
			wrapper.ReloadPlacement(wrapper.GetPlacementId(name), force);
		}

		public void StartPlacementAutoReload(string name)
		{
			wrapper.StartPlacementAutoReload(wrapper.GetPlacementId(name));
		}

		public void StopPlacementAutoReload(string name)
		{
			wrapper.StopPlacementAutoReload(wrapper.GetPlacementId(name));
		}

		public bool hasAdForPlacement(string name)
		{
			return wrapper.HasAdForPlacement(wrapper.GetPlacementId(name));
		}

		public void SetPlacementAlignment(string name, IAATKitBannerAlignment alignment)
        {
            Android.Views.View placementView = wrapper.GetPlacementView(wrapper.GetPlacementId(name));
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
            AlignPlacement(alignment, layoutParams);
            placementView.LayoutParameters = layoutParams;
        }

        public void SetPlacementAlignmentWithOffset(string name, IAATKitBannerAlignment alignment, int x, int y)
        {
            Android.Views.View placementView = wrapper.GetPlacementView(wrapper.GetPlacementId(name));
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
            AlignPlacement(alignment, layoutParams);

            if(x > 0)
            {
                layoutParams.LeftMargin = x;
            }
            else if(x < 0)
            {
                layoutParams.RightMargin = -x;
            }

            if (y > 0)
            {
                layoutParams.TopMargin = y;
            }
            else if (y < 0)
            {
                layoutParams.BottomMargin = -y;
            }

            placementView.LayoutParameters = layoutParams;
        }

        private static void AlignPlacement(IAATKitBannerAlignment alignment, FrameLayout.LayoutParams layoutParams)
        {
            switch (alignment)
            {
                case IAATKitBannerAlignment.TopLeft:
                    {
                        layoutParams.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.Top;
                    }
                    break;
                case IAATKitBannerAlignment.TopCenter:
                    {
                        layoutParams.Gravity = Android.Views.GravityFlags.CenterHorizontal | Android.Views.GravityFlags.Top;
                    }
                    break;
                case IAATKitBannerAlignment.TopRight:
                    {
                        layoutParams.Gravity = Android.Views.GravityFlags.Right | Android.Views.GravityFlags.Top;
                    }
                    break;
                case IAATKitBannerAlignment.BottomLeft:
                    {
                        layoutParams.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.Bottom;
                    }
                    break;
                case IAATKitBannerAlignment.BottomCenter:
                    {
                        layoutParams.Gravity = Android.Views.GravityFlags.CenterHorizontal | Android.Views.GravityFlags.Bottom;
                    }
                    break;
                case IAATKitBannerAlignment.BottomRight:
                    {
                        layoutParams.Gravity = Android.Views.GravityFlags.Right | Android.Views.GravityFlags.Bottom;
                    }
                    break;
            }
        }

        public void SetPlacementPosition(string name, int x, int y)
		{
			Android.Views.View placementView = wrapper.GetPlacementView(wrapper.GetPlacementId(name));
			FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
			layoutParams.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.Top;
			layoutParams.LeftMargin = x;
			layoutParams.TopMargin = y;
			placementView.LayoutParameters = layoutParams;
		}

		public void ShowPlacement(string name)
		{
			wrapper.ShowPlacement(wrapper.GetPlacementId(name));
		}

		// AATKit Events
		public event EventHandler<AATKitEventArgs> AATKitHaveAdEvent;
		public event EventHandler<AATKitEventArgs> AATKitHaveAdOnMultiSizeBannerEvent;
		public event EventHandler<AATKitEventArgs> AATKitNoAdsEvent;
		public event EventHandler<AATKitEventArgs> AATKitShowingEmptyEvent;
		public event EventHandler<AATKitEventArgs> AATKitPauseForAdEvent;
		public event EventHandler<AATKitEventArgs> AATKitResumeAfterAdEvent;
		public event EventHandler<AATKitEventArgs> AATKitEarnedIncentiveEvent;
        public event EventHandler<AATKitEventArgs> AATKitObtainedAdRulesEvent;
        public event EventHandler<AATKitEventArgs> AATKitManagedConsentNeedsUserInterface;
        public event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFinished;
        public event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFailedToLoad;
        public event EventHandler<AATKitEventArgs> AATKitManagedConsentCMPFailedToShow;

        public void AatkitHaveAd(int placementId)
		{
		    if (AATKitHaveAdEvent != null)
				AATKitHaveAdEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitHaveAdForPlacementWithBannerView(int placementId, Com.Addapptr.Aatkit_xamarin_wrapper.MultiSizeBanner placementView)
		{
			if (multiSizeBanner != null)
			{
				multiSizeBanner.Destroy();
			}
			multiSizeBanner = placementView;

			FrameLayout mainLayout = (FrameLayout)activity.Window.DecorView.FindViewById(Android.Resource.Id.Content);
			FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
			layoutParams.Gravity = Android.Views.GravityFlags.CenterHorizontal | Android.Views.GravityFlags.Bottom;
			mainLayout.AddView(multiSizeBanner.BannerView(), layoutParams);

			if (AATKitHaveAdOnMultiSizeBannerEvent != null)
				AATKitHaveAdOnMultiSizeBannerEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitNoAd(int placementId)
		{
		    if (AATKitNoAdsEvent != null)
				AATKitNoAdsEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitShowingEmpty(int placementId)
		{
			if (AATKitShowingEmptyEvent != null)
				AATKitShowingEmptyEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitPauseForAd(int placementId)
		{
			if (AATKitPauseForAdEvent != null)
				AATKitPauseForAdEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitResumeAfterAd(int placementId)
		{
			if (AATKitResumeAfterAdEvent != null)
				AATKitResumeAfterAdEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitUserEarnedIncentive(int placementId)
		{
			if (AATKitEarnedIncentiveEvent != null)
				AATKitEarnedIncentiveEvent(this, new AATKitEventArgs(wrapper.GetPlacementName(placementId)));
		}

		public void AatkitObtainedAdRules(bool obtainedRules)
		{
			if (AATKitObtainedAdRulesEvent != null)
			{
				string rules = obtainedRules ? "true" : "false";
				AATKitObtainedAdRulesEvent(this, new AATKitEventArgs(rules));
			}
		}

        public void AatkitManagedConsentNeedsUserInterface()
        {
            if (AATKitManagedConsentNeedsUserInterface != null)
                AATKitManagedConsentNeedsUserInterface(this, null);
        }

        public void AatkitManagedConsentCMPFailedToLoad(string error)
        {
            if(AATKitManagedConsentCMPFailedToLoad != null)
            {
                AATKitManagedConsentCMPFailedToLoad(this, new AATKitEventArgs(error));
            }
        }

        public void AatkitManagedConsentCMPFailedToShow(string error)
        {
            if(AATKitManagedConsentCMPFailedToShow != null)
            {
                AATKitManagedConsentCMPFailedToShow(this, new AATKitEventArgs(error));
            }
        }

        public void AatkitManagedConsentCMPFinished()
        {
            if(AATKitManagedConsentCMPFinished != null)
            {
                AATKitManagedConsentCMPFinished(this, null);
            }
        }
    }
}